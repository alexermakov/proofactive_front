$(function () {

    $(window).on("load", function () {
        $('body').addClass('loaded')
    })

    Fancybox.bind('.js__modal', {
        autoFocus: false,
        trapFocus: false,
        touch: false,
        closeButton: 'outside',
    });



    var _locale_en_airdatepicker = {
        days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
        months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        today: 'Today',
        clear: 'Clear',
        dateFormat: 'MM/dd/yyyy',
        timeFormat: 'hh:mm aa',
        firstDay: 0
    };


    $('.js_proof_modal__close').click(function(){
        $('.js_proof_modal__booking').removeClass('active')
    })


    function datePickerRegistration(){
        var minDate = new Date();

        new AirDatepicker('.js_proof_calendar_modal', {
            locale: _locale_en_airdatepicker,
            isMobile:true,
            minDate: minDate,
            inline: true,
            onSelect({date, formattedDate, datepicker}) {
                console.log(date)
                console.log(formattedDate)
                $('.js_proof_modal__date__time').removeClass('--disabled')
                $('.js_modal__last__date').text(formattedDate)


            }
        })
    }


    let bookingData = {};
    let bookingFormOpenForm = false;

    $('[data-show_booking]').click(function(){
        bookingFormOpenForm = false;
        showBooking()
    })

    function showBooking(){
        $('.js_proof_modal__booking').addClass('active')
        bookingNextStep(1)
    }

    $('.js_proof_modal__date__time input').change(function(){
        $('.js_modal__last__time').text($(this).val())

        if (bookingFormOpenForm){
            submitBooking()
        }
        else{
            bookingNextStep(2)
        }
    })

    function submitBooking(){
        console.log('submit')
        // тут нужно собрать данные и сделать отправку - после делаем показ последнего шага
        bookingNextStep(3)
    }




    $('.js_form__nookin_in_modal').submit(function (event) {
        if ($(this)[0].checkValidity()) {
            let formData = $(this).serialize();
            bookingFormOpenForm = true;
            submitBooking()
        }
    });

    $('.js_show_booking').submit(function (event) {
        if ($(this)[0].checkValidity()) {
            bookingData =  new FormData($(this)[0])
            console.log(bookingData);
            bookingFormOpenForm = true;

            showBooking();
        }
    });



    function bookingNextStep(step){
        $('.js_proof_modal__list .proof_modal__step.active').removeClass('active')
        $('.js_proof_modal__list .proof_modal__step[data-step="'+step+'"]').addClass('active')
    }

    datePickerRegistration()



    $('.js_help__content__top__tabs .help__content__top__tab').click(function(e){
        e.preventDefault()
        if (!$(this).hasClass('active')){

            $('.js_help__content__top__tabs .help__content__top__tab.active').removeClass('active')
            $(this).addClass('active')
            let inde = $(this).index();
            $('.js_help__content__section .help__content__block.active').fadeOut(400,function(){
                $(this).removeClass('active')
                $('.js_help__content__section .help__content__block').eq(inde).fadeIn(400,function(){
                    $(this).addClass('active')
                })
            })
        }
    })

    $('.js_help__content__block__tabs .help__content__block__tab').click(function(e){
        let thisTab = $(this).closest('.js_help__content__block__tabs')
        e.preventDefault()
        if (!$(this).hasClass('active')){
            thisTab.find('.help__content__block__tab.active').removeClass('active')
            $(this).addClass('active')
            let inde = $(this).index();

            let parent = $(this).closest('.js_help__content__block');
            parent.find('.help__content__block__item.active').fadeOut(400,function(){
                $(this).removeClass('active')
                parent.find('.help__content__block__item').eq(inde).fadeIn(400,function(){$(this).addClass('active')})
            })
        }
    })

    $('.js_help__accordion__menu a').click(function(e){
        e.preventDefault()
        if (!$(this).hasClass('active')){
            $(this).closest('.js_help__accordion__menu').find('a.active').removeClass('active')
            $(this).addClass('active')

            let inde = $(this).closest('li').index();

            let parent = $(this).closest('.js_help__accordion');
            parent.find('.js_help__accordion__main .help__accordion__section.active').fadeOut(400,function(){
                $(this).removeClass('active')
                parent.find('.js_help__accordion__main .help__accordion__section').eq(inde).fadeIn(400,function(){$(this).addClass('active')})
            })
        }
    })

    $('.js_help__accordion__section__list .help__accordion__line__top').click(function(e){
        e.preventDefault()
        let el = $(this)

        if (!$(this).closest('.help__accordion__line').hasClass('active')){
            let lastActive = $('.js_help__accordion__section__list .help__accordion__line.active');
            console.log($($(this)[0]))
            console.log(lastActive.find('.help__accordion__line__top'))
            lastActive.find('.help__accordion__line__content').slideUp(400);
            lastActive.removeClass('active')
        }
        el.closest('.help__accordion__line').find('.help__accordion__line__content').slideToggle(400,function(){
            el.closest('.help__accordion__line').toggleClass('active')
        })
    })



    if ($('.js_price__choose__slider__block').length){

        $('.js_price__choose__slider__block').each(function(){

            let $inputPrice = $(this).find('.js_count_price');
            let $rangeSlidertPrice = $(this).find('.js_rangeSlider__price');


            let rangeSlidertPriceX = $rangeSlidertPrice[0]
            noUiSlider.create($rangeSlidertPrice[0], {
                start: [$rangeSlidertPrice.data('start')],
                step: $rangeSlidertPrice.data('step'),
                connect: 'lower',
                range: {
                    'min': $rangeSlidertPrice.data('min'),
                    'max': $rangeSlidertPrice.data('max'),
                }
            });


            rangeSlidertPriceX.noUiSlider.on('slide', function (values) {
                $inputPrice.val(parseInt(values[0]))
                priceLimit()
            });


            $inputPrice.change(function (e) {
                e.preventDefault();
                rangeSlidertPriceX.noUiSlider.set($(this).val());
                priceLimit()
            });


        })




        function priceLimit(count){
            let maxCountTable = 0;

            proofCountArray = [];
            $('.js_price__choose__slider__block .js_count_price').each(function (index, element) {
                proofCountArray.push($(this).val())
            });
            let proofCount = proofCountArray.reduce((sum, current) => sum * current, 1) / 10;
            console.log(proofCount)
            $('.js_price__table__th__point').each(function (index, element) {
                let max_employees = parseInt($(this).attr('data-max-employees'));
                console.log('max_employees '+max_employees)
                console.log(proofCount)

                let $btn = $(this).find('.js_price__btn');
                let $notification = $(this).find('.price__table__th__notification');
                let $pointPrice = $(this).find('.price__table__th__point__price');

                if (proofCount>max_employees){
                    $pointPrice.hide(0)
                    $notification.show(0)
                    $btn.text($btn.attr('data-textlimit'))
                    $btn.prop("disabled", true);
                }else{
                    $notification.hide(0)
                    $pointPrice.show(0)
                    $btn.text($btn.attr('data-textsimple'))
                    $btn.prop("disabled", false);
                }
                if (maxCountTable<max_employees) maxCountTable=max_employees
            });

            let $customPlan = $('.price__table__th__notification--choose__all');
            if (maxCountTable<proofCount){
                $customPlan.show(0)
            }else{
                $customPlan.hide(0)
            }
        }


    }




    $('.js_home_faq__list .home_faq__item .home_faq__item__top').click(function(){
        $(this).closest('.home_faq__item').toggleClass('active')
        $(this).closest('.home_faq__item').find('.home_faq__item__content').slideToggle();
    })

    $('[data-btn_login_next').click(function(e){
        e.preventDefault();
        let $activeStep = $('.js_login_form__step__list .login_form__step.active');
        $activeStep.removeClass('active');
        $activeStep.next().addClass('active');
    })

    $('[data-btn_go_to_quize').click(function(e){
        e.preventDefault();
        let name = $('.js_registration_name').val();
        $('.js_quiz_registrion_step__title').text(name)
        let $activeStep = $('.js_login_form__step__list .login_form__step.active');
        $activeStep.removeClass('active');
        $('.js_form__registation_quize').css('display','flex')
        $('.js_login__page__content__top').hide(0)
    })





    $('.js_password').each(function (index, element) {
      $(this).addClass('field_x--password')
      $(this).append('<div class="btn_show_field_x--password js_btn_show_field_x--password"></div>')
    });

    $(document).on('click','.js_btn_show_field_x--password',function(){
        $(this).toggleClass('show')
        let $input = $(this).closest('.js_password').find('input')

        if ($input.attr('type') == 'text'){
            $input.attr('type','password')
        }else{
            $input.attr('type','text')
        }
    })

    $(".js_select").each(function () {
        let placeholder = $(this).attr('placeholder');
        $(this).select2({
            minimumResultsForSearch: 1 / 0,
            placeholder: placeholder,
            // allowClear: true
        })
    });


    $('.js__btn_close__modal').click(function (e) {
        Fancybox.close();
    });


    $('.js__btn_open__modal').click(function (e) {
        Fancybox.show([{
            src: "#js_modal__call"
        }])
    });


    $('.js_btn_menu,.js_modal__menu__btn_close,.js_modal__menu_overlay').click(function (e) {
        e.preventDefault();
        $('.js_btn_menu').toggleClass('active')
        $('.js__modal__menu').toggleClass('active')
        $('.js_modal__menu_overlay').toggleClass('active')
    });


    $('.js_btn_cookie__modal').click(function(e){
        e.preventDefault()
        $('.js_cookie__modal').addClass('hide_cookie')
        setTimeout(() => {
            $('.js_cookie__modal').remove()
        }, 250);
    })

});
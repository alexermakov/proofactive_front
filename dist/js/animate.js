$(function () {
    let lastScrollTop = 0;
	$(window).scroll(function () {
		const $header = $('.js_header');
		let st = $(this).scrollTop();



		let addClassHide = st > lastScrollTop ? true : false;

		if (st < $(window).height() / 10) {
			addClassHide = false;
		}


		if (addClassHide) {
			$header.addClass('header_hide')
		} else {
			$header.removeClass('header_hide')
		}
		lastScrollTop = st;
	});



    $(".__animate__top").each(function () {
        el = $(this)
        elParent = $(this)[0];
        let delay = $(this).data('data-delay') ? $(this).attr('data-delay') :0;

        gsap.to(el, {
            scrollTrigger: {
                trigger: elParent,
                start: 'top 90%',
                scrub: 1
            },
            y: '0',
            opacity:1,
            delay:delay,
            ease: "ease-in-out",
            duration: 0.75

        })
    })

    $(".__animate__right").each(function () {
        el = $(this)
        elParent = $(this)[0];
        let delay = $(this).data('data-delay') ? $(this).attr('data-delay') :0;

        gsap.to(el, {
            scrollTrigger: {
                trigger: elParent,
                start: 'top 50%',
                end: `+=150`,
                scrub: 1
            },
            x: '0',
            delay:delay,
            opacity:1,
            ease: "ease-in-out",
            duration: 0.75,
            stagger: {
                amount: 0.3
            }

        })
    })



    $(".__animate__opacity").each(function () {
        el = $(this)
        elParent = $(this).parent()[0];

        gsap.to(el, {
            scrollTrigger: {
                trigger: elParent,
                start: 'top 90%',
            },
            y: '0',
            opacity:1,
            ease: "ease-in-out",
            duration: 1

        })
    })


    $(".__animate__scale_down").each(function () {
        el = $(this);
        elParent = $(this)[0];

        gsap.to(el, {
            scrollTrigger: {
                trigger: elParent,
                start: 'top 95%',
            },
            scale: 1,
            ease: "ease-in-out",
            stagger: {
                amount: 0.3
            },
            duration: 1

        })
    })





    $("[data-panel-preview-container]").each(function () {
        el = $(this)
        let elParent = $('.__animate__home_promo')[0];
        let endSize = $(window).width()>800 ? 400: 120;
        gsap.to(el, {
            scale: '1',
            opacity:'1',
            ease: "ease-in-out",
            scrollTrigger: {
                trigger: elParent,
                start: "top 10%",
                end: `+=${endSize}`,
                scrub: 1,
                pin:true,
                onUpdate: self => {
                    console.log(self.progress)
                    if(self.progress > 0.4) {
                        let index = 1;
                        $("[data-animated-chart] rect").each(function () {
                            $(this)[0].style.transitionDelay = `${index*60}ms`;
                            index++
                        })

                        $("[data-animated-chart]").addClass('animated');
                        $("[data-animated-digits]").addClass('animated');
                    }else{
                        $("[data-animated-chart] rect").removeAttr('style')
                        $("[data-animated-chart]").removeClass('animated');
                        $("[data-animated-digits]").removeClass('animated');
                    }
                  }
            },
        })
    })


    $(".__animate__home__triggers__item").each(function () {
        el = $(this)
        let elParent = $(this).parent()[0];
        gsap.to(el, {
            scale: '1',
            opacity:'1',
            ease: "ease-in-out",
            scrollTrigger: {
                trigger: elParent,
                start: "top 80%",
                end: `+=100`,
                onUpdate: self => {
                    console.log(self.progress)
                    if(self.progress > 0.5) {
                        $(".__animate__home__triggers__item").each(function (index, element) {
                            setTimeout(() => {
                                $(this).addClass('active');
                            }, 700*index);
                        });
                    }else{
                        $(".__animate__home__triggers__item").removeClass('active');
                    }
                  }
            },
        })
    })


    $("[data-panel-preview-image]").each(function () {
        el = $(this)
        elParent = $('.__animate__home_promo')[0];
        let endSize = $(window).width()>800 ? 400: 120;
        gsap.to(el, {
            opacity:'1',
            ease: "ease-in-out",
            scrollTrigger: {
                trigger: elParent,
                start: "top 10%",
                end: `+=${endSize}`,
                scrub: 1,
                pin:true
            },
        })
    })






    $(".__animate__home_card--1").each(function () {
        el = $(this)
        elParent = $('.__animate__home_card')[0];
        gsap.to(el, {
            left: '7.5%',
            ease: "cubic-bezier(.19,1,.22,1)",
            scrollTrigger: {
                trigger: elParent,
                start: "top 80%",
                end: "top 0%",
                scrub: 1
            }
        })
    })

    $(".__animate__home_card--2").each(function () {
        el = $(this)
        elParent = $('.__animate__home_card')[0];
        gsap.to(el, {
            left: '22.5%',
            ease: "ease-in-out",
            scrollTrigger: {
                trigger: elParent,
                start: "top 80%",
                end: "top 0%",
                scrub: 1
            }
        })
    })



    $(".__animate__home_card--3").each(function () {
        el = $(this)
        elParent = $('.__animate__home_card')[0];
        gsap.to(el, {
            left: '40%',
            ease: "ease-in-out",
            scrollTrigger: {
                trigger: elParent,
                start: "top 80%",
                end: "top 0%",
                scrub: 1
            }
        })
    })

    $(".__animate__home_card--4").each(function () {
        el = $(this)
        elParent = $('.__animate__home_card')[0];
        gsap.to(el, {
            left: '55%',
            ease: "ease-in-out",
            scrollTrigger: {
                trigger: elParent,
                start: "top 80%",
                end: "top 0%",
                scrub: 1
            }
        })
    })

    $(".__animate__home_card--5").each(function () {
        el = $(this)
        elParent = $('.__animate__home_card')[0];
        gsap.to(el, {
            left: '72.5%',
            ease: "ease-in-out",
            scrollTrigger: {
                trigger: elParent,
                start: "top 80%",
                end: "top 0%",
                scrub: 1
            }
        })
    })

});
$(function () {

    $('.js_quiz_registrion_step1 input').change(function (e) {
        e.preventDefault();
        let $data_Step1_Owner = $('[data-step1-owner]');
        let $data_Step1_Personal = $('[data-step1-personal]');
        let $data_Step1_Business = $('[data-step1-business]');
        let $data_Step1_Dispatcher = $('[data-step1-dispatcher]');

        let type = $(this).attr('data-type');
        console.log(type)
        // show owner add
        if (type=='owner'){
            $data_Step1_Owner.show(0)
        }else{
            $data_Step1_Owner.hide(0)
        }

         // show personal
        if (type=='personal'){
            $data_Step1_Personal.show(0)
        }else{
            $data_Step1_Personal.hide(0)
        }

         // show business
        if (type=='business'){
            $data_Step1_Business.show(0)
        }else{
            $data_Step1_Business.hide(0)
        }

         // show dispatcher
        if (type=='dispatcher'){
            $data_Step1_Dispatcher.show(0)
        }else{
            $data_Step1_Dispatcher.hide(0)
        }
    });



    $('[data-type-dispatcher_step_2').click(function(e){
        e.preventDefault();
        $('[data-step-1]').hide(0).removeClass('active')
        $('[data-step-2-owner]').show(0).addClass('active')
    })

    $('[data-type-owner_step_2').click(function(e){
        e.preventDefault();
        $('[data-step-1]').hide(0).removeClass('active')
        $('[data-step-2-dispatcher]').show(0).addClass('active')
    })

    $('[data-registration-back').click(function(e){
        e.preventDefault();
        $('[data-step-1]').show(0).addClass('active')
        $('[data-step-2-dispatcher]').hide(0).removeClass('active')
        $('[data-step-2-owner]').hide(0).removeClass('active')
    })








    $('[data-registration-submit').click(function(e){
        e.preventDefault();
        submitRegistration()
    })


    function submitRegistration(){
        console.log('submitRegistration');
        location.reload();
    }


})